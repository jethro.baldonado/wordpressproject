<div class="container-fluid p-0">
    <div class="shop-banner w-100">
        <?php 
            $apparelBannerCustomPost = get_field("apparel_banner", get_the_ID());
            $apparelBanner = $apparelBannerCustomPost ? $apparelBannerCustomPost : "https://via.placeholder.com/3840x1200";
        ?>
        <figure style="max-height: 300px; overflow: hidden">
            <img src="<?php echo $apparelBanner ?>" class="w-100" style="margin: -15% 0;"/>
        </figure>
    </div>
    <div class="row m-0">
        <div class="col-sm-12 col-md-6 p-0">
            <?php 
                $apparelCustomPost = get_field("apparel_image", get_the_ID());
                $apparelImg = $apparelCustomPost ? $apparelCustomPost : "https://via.placeholder.com/3840x1200";
            ?>
                <img src="<?php echo $apparelImg ?>" class="w-100" style="height:100%" />
        </div>
        <div class="col-sm-12 col-md-6">
            <div class="col-sm-12 col-md-10 ec-padding">
                <?php dynamic_sidebar('sidebar-apparel'); ?>
                <a href="" class="btn btn-white"><i class="fas fa-shopping-cart"></i> Shop Now</a>
            </div>
        </div>
    </div>
</div>