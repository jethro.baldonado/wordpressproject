<?php get_header(); ?>
<div class="container-fluid">
    <?php get_sidebar("newsletter"); ?>
</div>
<div id="about-me" class="container">
    <div class="row justify-content-end">
        
        <?php 
            if(have_posts()) {
                while(have_posts()) {
                    the_post();
                    ?>
        <div class="col-md-4">
            <img src="<?php echo get_the_post_thumbnail_url(get_the_ID()) ?>">
        </div>
        <div class="col-sm-12 col-md-8">
            <h1 class="col-sm-12">
                <?php the_title(); ?>
            </h1>
            <div class="col-sm-12">
                <?php the_content(); ?>
            </div>
        </div>
        <?php   }
            }
        ?>
    </div>
</div>
<div class="container-fluid p-0">
    <div class="gallery-widget text-center">
        <div class="gallery-inner">
            <h3>Gallery</h3>
            <div class="gallery-carousel">
                <div class="gallery-item">
                    <img src="https://via.placeholder.com/1200x1200" alt="">
                </div>
                <div class="gallery-item">
                    <img src="https://via.placeholder.com/1200x1200" alt="">
                </div>
                <div class="gallery-item">
                    <img src="https://via.placeholder.com/1200x1200" alt="">
                </div>
                <div class="gallery-item">
                    <img src="https://via.placeholder.com/1200x1200" alt="">
                </div>
                <div class="gallery-item">
                    <img src="https://via.placeholder.com/1200x1200" alt="">
                </div>
                <div class="gallery-item">
                    <img src="https://via.placeholder.com/1200x1200" alt="">
                </div>
                <div class="gallery-item">
                    <img src="https://via.placeholder.com/1200x1200" alt="">
                </div>
                <div class="gallery-item">
                    <img src="https://via.placeholder.com/1200x1200" alt="">
                </div>
                <div class="gallery-item">
                    <img src="https://via.placeholder.com/1200x1200" alt="">
                </div>
                <div class="gallery-item">
                    <img src="https://via.placeholder.com/1200x1200" alt="">
                </div>
                <div class="gallery-item">
                    <img src="https://via.placeholder.com/1200x1200" alt="">
                </div>
                <div class="gallery-item">
                    <img src="https://via.placeholder.com/1200x1200" alt="">
                </div>
            </div>
        </div>
        <a href="/gallery" class="btn btn-black" id="dis">Discover More</a>
    </div>
</div>
<?php 
    get_sidebar('apparel');
?>
<div id="podcast-widget" class="container-fluid p-0 position-relative z-index-1">
    <div class="shop-banner w-100 position-absolute">
        <img src="<?php echo get_field("podcast_banner") ?>" class="w-100" />
    </div>
    <div class="container">
        <h1 class="text-center p-3 text-white position-relative">Podcasts</h1>
        <div class="row clearfix">
            
        <?php
            $rssUri = get_field('feed_url', get_page_by_path( 'podcast' )->ID);
            $rss = fetch_feed($rssUri);
            $rssItems = [];
            if ( ! is_wp_error( $rss ) ) : // Checks that the object is created correctly

                // Figure out how many total items there are, but limit it to 5. 
                $maxItems = $rss->get_item_quantity( 3 ); 
            
                // Build an array of all the items, starting with element 0 (first element).
                $rssItems = $rss->get_items( 0, $maxItems );
                
            endif;
            if(!empty($rssItems)) {
                foreach($rssItems as $item) { ?>
                <?php 
                    $imageArr = $item->get_item_tags(SIMPLEPIE_NAMESPACE_ITUNES, 'image');
                    $imageUrl = $imageArr[0]['attribs'][""]["href"];
                ?>
                <div class="podcast-item col-sm-12 col-md">
                    <div class="podcast-thumb">
                        <img src="<?php echo $imageUrl; ?>" class="w-100" />
                    </div>
                    <div class="podcast-content">
                        <div class="podcast-title">
                            <h4><?php echo esc_html( $item->get_title() ); ?></h4>
                            <small>Date: <?php echo esc_html( $item->get_date()); ?></small>
                        </div>
                        <div class="podcast-summary">
                            <p>
                                <?php echo $item->get_description(); ?>
                            </p>
                            <a href="<?php echo $item->get_permalink(); ?>" class="read-more-link">Read More</a>
                        </div>
                    </div>
                </div>
            <?php }
            }
        ?>
        </div>
        <div class="row mt-60">
            <div class="col-sm-12 text-center">
                <a href="/podcast" class="more-link">More Podcasts</a>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>
