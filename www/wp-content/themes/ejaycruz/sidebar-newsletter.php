<?php 
    if(!is_active_sidebar('sidebar-newsletter')) return;
    
    $pageID = get_option('page_on_front');
    $newsLetterBanner = get_field('stay_in_touch_image', $pageID);
?>
<div id="newsletter" style="background: url(<?php echo $newsLetterBanner; ?>);">
    <div class="col-md-6"></div>
    <div class="col-sm-12 col-md-6">
        <?php dynamic_sidebar('sidebar-newsletter'); ?>
    </div>
</div>
