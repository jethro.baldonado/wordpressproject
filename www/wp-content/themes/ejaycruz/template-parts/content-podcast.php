<div class="container-fluid p-0 banner-container">
	<?php 
	?>
    <img src="<?php echo get_the_post_thumbnail_url() ? get_the_post_thumbnail_url() : 'https://via.placeholder.com/3840x600' ?>" alt="">
</div>
<div id="<?php echo $post->post_name; ?>" class="container-fluid ec-padding px-0">
    <h1 class="text-center">
        <?= get_the_title(); ?>
    </h1>
    <div class="d-flex flex-wrap ">
        <div class="p-0 col-sm-12 col-md-3">
            <img src="<?php echo get_field('podcast_first_banner') ? get_field('podcast_first_banner') : "https://via.placeholder.com/500x500"; ?>" alt="" width="476" height="476">
        </div>
        <div class="valign-middle p-0 bg-dark text-white col-sm-12 col-md-3 align-middle text-center">
            <h2><?php echo get_field('podcast_title') ?></h2>
        </div>
        <div class="p-0 col-md-3 col-sm-12">
            <img src="<?php echo get_field('podcast_second_banner') ? get_field('podcast_second_banner') : "https://via.placeholder.com/500x500"; ?>" alt="" width="476" height="476">
        </div>
        <div class="valign-middle p-0 bg-dark text-white col-md-3 col-sm-12">
            <div>
                <p class=" ">
                    <?= get_the_content(); ?>
                </p>
                <a href="">Visit Site</a>
            </div>
        </div>
    </div>
</div>
<div class="container music-quicklink">
    <ul class="nav justify-content-center">
        <li class="nav-item">
            <a class="nav-link" href="https://www.youtube.com/channel/UCOsHFbueGDVGuyMHQ3IAoBw">
                <img src="<?php echo get_template_directory_uri(). '/images/youtube.png'; ?>" />
            </a>
        </li>
        <li class="nav-item">
        <a class="nav-link" href="https://itunes.apple.com/us/podcast/the-parables/id1437460025?mt=2">
                <img src="<?php echo get_template_directory_uri(). '/images/itunes.png'; ?>" />
            </a>
        </li>
        <li class="nav-item">
        <a class="nav-link" href="https://open.spotify.com/show/7eDFbY6BBbA4b7CuM4OUJt?si=dSSM67LaRgWiwCngqHTPDQ">
                <img src="<?php echo get_template_directory_uri(). '/images/spotify.png'; ?>" />
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="http://www.stitcher.com/s?fid=238824&refid=stpr">
                <img src="<?php echo get_template_directory_uri(). '/images/stitcher.png'; ?>" />
            </a>
        </li>
        <li class="nav-item">
        <a class="nav-link" href="https://castbox.fm/vc/1443702">
                <img src="<?php echo get_template_directory_uri(). '/images/castbox.png'; ?>" />
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="http://tun.in/pje0p">
                <img src="<?php echo get_template_directory_uri(). '/images/tunein.png'; ?>" />
            </a>
        </li>
        <li class="nav-item">
        <a class="nav-link" href="http://www.buzzsprout.com/213410">
                <img src="<?php echo get_template_directory_uri(). '/images/buzzsprout.png'; ?>" />
            </a>
        </li>
    </ul>
</div>
<div id="podcast-widget" class="container bg-white">
    <div class="row">
    <?php
        $rssUri = get_field('feed_url');
        $rss = fetch_feed($rssUri);
        $rssItems = [];
        if ( ! is_wp_error( $rss ) ) : // Checks that the object is created correctly

            // Figure out how many total items there are, but limit it to 5. 
            $maxItems = $rss->get_item_quantity(); 
        
            // Build an array of all the items, starting with element 0 (first element).
            $rssItems = $rss->get_items( 0, $maxItems );
            
        endif;
        if(!empty($rssItems)) {
            foreach($rssItems as $item) { ?>
            <?php 
                $imageArr = $item->get_item_tags(SIMPLEPIE_NAMESPACE_ITUNES, 'image');
                $imageUrl = $imageArr[0]['attribs'][""]["href"];
            ?>
            <div class="podcast-item col-sm-12 col-md-6 justify-content-between ">
                <div class="podcast-thumb">
                    <img src="<?php echo $imageUrl; ?>" class="w-100" />
                </div>
                <div class="podcast-content">
                    <div class="podcast-title">
                        <h4><?php echo esc_html( $item->get_title() ); ?></h4>
                        <small>Date: <?php echo esc_html( $item->get_date()); ?></small>
                    </div>
                    <div class="podcast-summary">
                        <p>
                            <?php echo $item->get_description(); ?>
                        </p>
                        <a href="<?php echo $item->get_permalink(); ?>" class="read-more-link">Read More</a>
                    </div>
                </div>
            </div>
        <?php }
        }
    ?>
    </div>
            <div class="row mt-60">
        </div>
    </div>
</div>