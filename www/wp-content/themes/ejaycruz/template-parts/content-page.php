<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package EjayCruz
 */

?>
<div class="container-fluid p-0 banner-container">
	<?php 
	?>
    <img src="<?php echo get_the_post_thumbnail_url() ? get_the_post_thumbnail_url() : 'https://via.placeholder.com/3840x600' ?>" alt="">
</div>
<article id="post-<?php the_ID(); ?>" <?php post_class('container'); ?>>
	<header class="entry-header text-center">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
	</header><!-- .entry-header -->
	<div class="entry-content <?php echo $post->post_name; ?>">
		<?php
		the_content();

		wp_link_pages( array(
			'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'ejaycruz' ),
			'after'  => '</div>',
		) );
		?>
	</div><!-- .entry-content -->
</article><!-- #post-<?php the_ID(); ?> -->
