<?php get_header(); ?>
<?php
		while ( have_posts() ) :
			the_post();
?>
<?php 
	if (locate_template( array( 'template-parts/content-' . $post->post_name . '.php' ) ) != '') {
		// yep, load the page template
		get_template_part('template-parts/content', $post->post_name);
	} else {
		// nope, load the content
		get_template_part( 'template-parts/content', 'page' );
	}
?>	
<?php 
	endwhile; // End of the loop.
?>
<?php get_footer(); ?>