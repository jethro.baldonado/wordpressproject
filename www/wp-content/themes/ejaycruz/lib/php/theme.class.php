<?php

class EjayCruzTheme
{
    /** @var bool */
    private static $booted = false;

    private static $config = [

    ];

    private static $instance;

    public static function boot($config = []) 
    {
        self::$instance = new self;
    }
    
    public static function isBooted() {
        return self::$booted;
    }

    public static function getInstance() 
    {
        return self::$instance;
    }

    public function __construct() 
    {
        //Hooks and Filters here
        add_action('widgets_init', [$this, 'theme_create_widgets']);
        add_action('wp_enqueue_scripts', [$this, 'registerVendorScripts']);
        add_action('get_search_form', [$this, 'searchForm']);
    }

    /** @method Widget Creation */
    public function theme_create_widgets() 
    {
        //Newsletter
        register_sidebar([
            'name' => __('Newsletter Sidebar', 'theme-slug'),
            'id' => 'sidebar-newsletter',
            'description' => __('This widget will contain the contents of the news letter'),
            'before_widget' => '<div class="widget-content">',
            'after_widget' => "</div>",
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        ]);
        
        //Social Media Widget
        register_sidebar([
            'name' => __('Navigation Sidebar', 'theme-slug'),
            'id' => 'sidebar-navigation',
            'description' => __('This sidebar will contain the list of widgets for the navigation part'),
            'before_widget' => '<div class="social-media">',
            'after_widget' => '</div>',
            'before_title' => '<div style="display: none">',
            'after_title' => '</div>'
        ]);

        register_sidebar([
            'name' => __('Search Sidebar', 'theme-slug'),
            'id' => 'sidebar-search',
            'description' => __('This sidebar is used for searching'),
            'before_widget' => '<div class="search">',
            'after_widget' => '</div>',
            'before_title' => '<div style="display: none;">',
            'after_title' => '</div>'
        ]);
        
        register_sidebar([
            'name' => __('Apparel & Accessories Sidebar', 'theme-slug'),
            'id' => 'sidebar-apparel',
            'description' => __('This sidebar is used for the contents of the apparel'),
            'before_widget' => '<div class="apparel-container">',
            'after_widget' => '</div>',
            'before_title' => '<h3>',
            'after_title' => '</h3>'
        ]);
    }
    public function registerVendorScripts() {
        //CSS
        wp_enqueue_style('slick_css', get_template_directory_uri() . '/vendors/slick/slick.css');
        wp_enqueue_style('slick_theme_css', get_template_directory_uri() . '/vendors/slick/slick-theme.css');

        //JS
        wp_enqueue_script('bootstrap', get_template_directory_uri() . '/vendors/bootstrap.min.js', ['jquery'], '1.12', true);
        $slickURL =  get_template_directory_uri() . '/vendors/slick/slick.js';
        wp_enqueue_script('slick_js', $slickURL, ['jquery'], '1.12', true);

        /** app.js shall be the last one to be added */        
        wp_enqueue_script('app_js', get_template_directory_uri() . '/assets/js/app.js', ['jquery'], '1.12', true);
    }
    public function searchForm() {
        get_template_part('template-parts/custom-search-form');
        return ""; 
    }
}