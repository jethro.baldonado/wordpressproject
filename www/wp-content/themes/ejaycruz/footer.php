<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package EjayCruz
 */

?>
</div><!-- #content -->
<div class="footer container-fluid p-0">
	<footer class="container">
		<div class="row">
			<div class="col-sm">Logo Here</div>
			<div class="col-sm">
				<h4>Navigate</h4>
				<ul>
					<li><a href="">About Me</a></li>
					<li><a href="">Gallery</a></li>
					<li><a href="">Shop</a></li>
					<li><a href="">Podcast</a></li>
				</ul>
				<ul>
					<li><a href="">Connect</a></li>
					<li><a href="">Blog</a></li>
					<li><a href="">Terms and Conditions</a></li>
					<li><a href="">Privacy Policy</a></li>
				</ul>
			</div>
			<div class="col-sm">
				<h4>Subscribe for Updates</h4>
				<input type="text" placeholder="Enter email address">
				<button class="btn btn-black border-radius-0">SUBSCRIBE</button>
			</div>
		</div>
	</footer>
	<footer class="container-fluid p-0 footer-line">
		<div class="container p-0">
			<div class="row">
				<div class="col-sm text-left">
					Copyright &copy; Ejay Cruz
				</div>
				<div class="col-sm text-right">

				</div>
			</div>
		</div>
	</footer>
</div>

</div><!-- #page -->

<?php wp_footer(); ?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/parallax.js/1.5.0/parallax.min.js"></script>
</body>

</html>