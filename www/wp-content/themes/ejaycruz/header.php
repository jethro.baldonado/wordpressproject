<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package EjayCruz
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<link href="https://fonts.googleapis.com/css?family=Montserrat|Prata|Raleway" rel="stylesheet">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<header id="masthead" class="site-header">
		<nav id="site-navigation" class="navbar navbar-expand-lg navbar-light text-center">
			<a class="navbar-brand" href="#">Ejay Cruz</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#headerNav" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="headerNav">
				<?php
					wp_nav_menu( array(
						'theme_location' => 'menu-1',
						'menu_id'        => 'primary-menu',
						'menu_class'     => 'nav justify-content-end'
					) );
				?>
				<li class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-137 current_page_item menu-item-139">
					<?= do_shortcode('[wps_cart]'); ?>
				</li>
				<?php get_search_form(); ?>
				<?php dynamic_sidebar('sidebar-navigation'); ?>
			</div>
		</nav><!-- #site-navigation -->
		
		<?php 
			if(is_front_page()) {
				?>
					<div class="banner-area">
						<?php 							
							$postMeta = get_field("banner_image", get_the_ID());
							$bannerImage = $postMeta ? $postMeta : "https://via.placeholder.com/3840x2160";
						?>
						<img src="<?php echo $bannerImage; ?>" />
						<div class="scroll-next-container">
							<a href="#newsletter" id="view-newsletter"><img src="data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTkuMC4wLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iTGF5ZXJfMSIgeD0iMHB4IiB5PSIwcHgiIHZpZXdCb3g9IjAgMCA1MTIgNTEyIiBzdHlsZT0iZW5hYmxlLWJhY2tncm91bmQ6bmV3IDAgMCA1MTIgNTEyOyIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSIgd2lkdGg9IjMycHgiIGhlaWdodD0iMzJweCI+CjxnPgoJPGc+CgkJPHBhdGggZD0iTTUwOS4xMjEsMTI1Ljk2NmMtMy44MzgtMy44MzgtMTAuMDU1LTMuODM4LTEzLjg5MywwTDI1Ni4wMDUsMzY1LjE5NEwxNi43NzEsMTI1Ljk2NmMtMy44MzgtMy44MzgtMTAuMDU1LTMuODM4LTEzLjg5MywwICAgIGMtMy44MzgsMy44MzgtMy44MzgsMTAuMDU1LDAsMTMuODkzbDI0Ni4xOCwyNDYuMTc1YzEuODQyLDEuODQyLDQuMzM3LDIuODc4LDYuOTQ3LDIuODc4YzIuNjEsMCw1LjEwNC0xLjAzNiw2Ljk0Ni0yLjg3OCAgICBsMjQ2LjE3LTI0Ni4xNzVDNTEyLjk1OSwxMzYuMDIxLDUxMi45NTksMTI5LjgwNCw1MDkuMTIxLDEyNS45NjZ6IiBmaWxsPSIjRkZGRkZGIi8+Cgk8L2c+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPC9zdmc+Cg==" /></a>
						</div>
					</div>		
				<?php
			}
		?>
	</header><!-- #masthead -->

	<div id="content" class="site-content">
