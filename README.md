# Installing Docker
Check the link below in installing docker
* [DockerInstall](https://docs.docker.com/compose/install/#install-compose) - Docker Compose installation

# Parsing SCSS

* Install dependencies first
```
npm install
```

* Running commands
Available commands
watch, dev, prod
```
yarn watch
```
or
```
npm run watch
```